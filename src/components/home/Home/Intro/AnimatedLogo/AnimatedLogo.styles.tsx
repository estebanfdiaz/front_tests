import styled from "styled-components";

import { AnimatedLogoStyledProps as Props } from "./AnimatedLogo.types";
import Logo from "../../../Logo/LogoStyled";

const AnimatedLogoStyled = styled(Logo)<Props>`
  .logo {
    &__circle {
      animation: App-logo-bounce infinite 5s 1s ease-in-out;
    }
  }
`;
export default AnimatedLogoStyled;
