import styled from "styled-components";

import { HomeStyledProps as Props } from "./Home.types";

const HomeStyled = styled.div<Props>`
  min-width: var(--sizes-page-minWidth);
  max-width: var(--sizes-page-maxWidth);
  min-height: var(--sizes-page-minHeight);
  display: flex;
  flex-direction: column;
  color: var(--palette-white-0);
  background-color: var(--palette-white-1);
  font-size: 1.8em;

  .Home {
    &__header {
    }

    &__main {
      flex: 1;
      display: flex;
      flex-direction: column;
    }

    &__button {
      margin-right: 2rem;
      display: flex;
      justify-content: flex-start;

      &--fruit {
        margin-right: 2rem;
        background-color: var(--palette-primary);

        &-column{
          margin: 1rem;
          background-color: var(--palette-primary);
          border-radius: 1rem;
        }
      }

      &--vegetable {
        margin-right: 2rem;
        background-color: var(--palette-vegetable);

        &-column{
          margin: 1rem;
          background-color: var(--palette-vegetable);
          border-radius: 1rem;
        }
      }

      &--milk {
        margin-right: 2rem;
        background-color: var(--palette-milk);

        &-column{
          margin: 1rem;
          background-color: var(--palette-milk);
          border-radius: 1rem;
        }
      }

      &--meat {
        margin-right: 2rem;
        background-color: var(--palette-meat);

        &-column{
          margin: 1rem;
          background-color: var(--palette-meat);
          border-radius: 1rem;
        }
      }
    }

    &__labelContainer {
      display: flex;
      flex-direction: row;
      justify-content: center;
      align-items: center;
      margin-top:3rem;
    }

    &__labelItemsContainer {
      display: flex;
      flex: 1;
      flex-direction: column;
      justify-content: flex-start;
      align-items: center;
      margin-top:3rem;
    }

    &__itemsContainer {
      width: 40%
    }

    &__title {
      margin: 2rem 2rem;
      height: 10rem;
      padding-top: 2rem;
    }

    &__footer {
    }
  }
`;

export default HomeStyled;
