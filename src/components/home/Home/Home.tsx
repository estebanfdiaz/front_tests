import React, { useState, useEffect } from "react";

import Styles from "./Home.styles";
import { HomeProps as Props } from "./Home.types";
import Button from "components/global/Button/Button";

const Home: React.FC<Props> = props => {
  const fruitArr: string[] = ["Manzanas", "Uvas", "Peras", "Mandarinas"];
  const vegetableArr: string[] = ["Lechuga", "Tomate", "Cebolla"];
  const meatArr: string[] = ["Chancho", "Res", "Pollo", "Cordero"];
  const milkArr: string[] = ["Leche", "Queso"];

  const [selectedItems, setSelectedItems] = useState<string[]>([]);
  const [colorByType, setColorByType] = useState<string>("");

  const handleDisplayItems = (type: string) => {
    setColorByType(type);
    switch (type) {
      case "fruit":
        setSelectedItems(fruitArr);
        break;
      case "vegetable":
        setSelectedItems(vegetableArr);
        break;
      case "meat":
        setSelectedItems(meatArr);
        break;
      case "milk":
        setSelectedItems(milkArr);
        break;
      default:
        setSelectedItems([]);
        break;
    }
  };

  useEffect(()=> {
    setColorByType("fruit")
    setSelectedItems(fruitArr);
  }, [])

  return (
    <Styles className="Home">
      <header className="Home__header" />
      <main className="Home__main">
        <div className="Home__labelContainer">
          <Button className="Home__button--fruit" onClick={() => handleDisplayItems("fruit")}>Frutas</Button>
          <Button className="Home__button--vegetable" onClick={() => handleDisplayItems("vegetable")}>Verduras</Button>
          <Button className="Home__button--meat" onClick={() => handleDisplayItems("meat")}>Carnes</Button>
          <Button className="Home__button--milk" onClick={() => handleDisplayItems("milk")}>Lacteos</Button>
        </div>
        <div className="Home__labelItemsContainer">
          <div className={"Home__itemsContainer"}>
            {selectedItems?.map((item: string) => 
              <div key={item} className={`Home__button--${colorByType}-column`}>
              <p className={"Home__title"}>{item}</p>
              </div>
            )}
          </div>
        </div>
      </main>
      <footer className="Home__footer" />
    </Styles>
  );
};

Home.defaultProps = {};

export default Home;
