import React from "react";
import { render } from "@testing-library/react";

import AnimatedLogoStyled from "./AnimatedLogo.styles";

describe("AnimatedLogo", () => {
  it("renders without crashing", () => {
    const { container } = render(
      <AnimatedLogoStyled className="AnimatedLogo" />
    );
    expect(container).toMatchSnapshot();
  });
});
