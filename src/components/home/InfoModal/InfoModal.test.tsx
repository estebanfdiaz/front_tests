import React from "react";

import { render } from "setupTests";
import InfoModal from "./InfoModal";

describe("InfoModal", () => {
  it("renders without crashing", () => {
    const { container } = render(<InfoModal />);
    expect(container).toMatchSnapshot();
  });
});
