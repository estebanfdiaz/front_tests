import React from "react";
import { useTranslation } from "react-i18next";

import { InfoModalProps as Props } from "./InfoModal.types";
import Styles from "./InfoModal.styles";

const InfoModal: React.FC<Props> = () => {
  const { t } = useTranslation();
  return (
    <Styles className="InfoModal">
      <h2 className="InfoModal__heading">{t("included")}</h2>
      <ul className="InfoModal__ul">
        <li className="InfoModal__li">React v16.12.0</li>
        <li className="InfoModal__li">Redux</li>
        <li className="InfoModal__li">Redux-thunk</li>
        <li className="InfoModal__li">Typescript v3.7</li>
        <li className="InfoModal__li">Styled-components v5</li>
        <li className="InfoModal__li">Babel v7</li>
        <li className="InfoModal__li">Webpack v4</li>
        <li className="InfoModal__li">Automatic CSS Prefixer</li>
        <li className="InfoModal__li">PWAs Manifest & Service Workers</li>
        <li className="InfoModal__li">React-testing-library v9</li>
        <li className="InfoModal__li">Jest v24</li>
        <li className="InfoModal__li">Cypress v3</li>
      </ul>
    </Styles>
  );
};

InfoModal.defaultProps = {};

export default InfoModal;
