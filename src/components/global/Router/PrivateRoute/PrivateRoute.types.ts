// Interfaces and types from component PrivateRoute

// Component Props
export interface PrivateRouteProps {
  component: (() => JSX.Element) | undefined;
  path: string;
  exact?: boolean;
}
