import React from "react";
import { Route, Redirect } from "react-router-dom";

import { PrivateRouteProps as Props } from "./PrivateRoute.types";
import CONSTANTS from "../../../../config/constants";
import { useSelector } from "redux/store";

const PrivateRoute: React.FC<Props> = props => {
  const { component, ...rest } = props;
  const loggedIn = useSelector(state => state.Auth.loggedIn);

  if (!component) {
    throw new Error("component must be defined");
  }
  const Component = component as () => JSX.Element;
  return (
    <Route
      component={(props: any) => {
        return loggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: CONSTANTS.NO_AUTH_PATH,
              state: { from: props.location }
            }}
          />
        );
      }}
      {...rest}
    />
  );
};

PrivateRoute.defaultProps = {
  exact: false
};

export default PrivateRoute;
