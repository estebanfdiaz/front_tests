import { History } from "history";
// Interfaces and types from component Router

// Component Props
export interface RouterProps {
  history: History;
}
