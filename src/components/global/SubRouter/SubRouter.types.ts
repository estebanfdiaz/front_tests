// Interfaces and types from component SubRouter

// Component Props
export interface Props {
  url: string;
  routes: SingleRoute[];
}

export interface SingleRoute {
  path: string;
  exact?: boolean;
  component?: () => JSX.Element;
  isPrivate?: boolean;
}
