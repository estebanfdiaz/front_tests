import React, { Suspense } from "react";
import { Route, Redirect, Switch } from "react-router-dom";

import { Props } from "./SubRouter.types";
import PrivateRoute from "../Router/PrivateRoute/PrivateRoute";

const SubRouter: React.FC<Props> = props => {
  const { url, routes } = props;

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <Switch>
        {routes.map(singleRoute => {
          const { path, exact, isPrivate = false, ...otherProps } = singleRoute;
          const fullPath = `${url}/${path}`.replace(/\/\/+/, "/");

          if (isPrivate) {
            const { component, ...rest } = otherProps;
            return (
              <PrivateRoute
                key={path}
                exact={exact}
                path={fullPath}
                component={component}
                {...rest}
              />
            );
          }

          return (
            <Route key={path} exact={exact} path={fullPath} {...otherProps} />
          );
        })}
        <Redirect to="/404" />
      </Switch>
    </Suspense>
  );
};

SubRouter.defaultProps = {};

export default SubRouter;
