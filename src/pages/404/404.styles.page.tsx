import styled from "styled-components";

import { FourZeroFourStyledProps as Props } from "./404.page.types";

const FourZeroFourStyled = styled.div<Props>`
  min-width: var(--sizes-page-minWidth);
  max-width: var(--sizes-page-maxWidth);
  min-height: var(--sizes-page-minHeight);
  display: flex;
  flex-direction: column;

  .FourZeroFour {
    &__header {
    }

    &__main {
      flex: 1;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    &__icon {
      stroke: var(--palette-primary);
    }

    &__footer {
    }
  }
`;

export default FourZeroFourStyled;
