import React from "react";

import Styles from "./404.styles.page";
import { FourZeroFourProps as Props } from "./404.page.types";

import { ReactComponent as FourZeroFourSVG } from "assets/images/404/404.svg";

const FourZeroFour: React.FC<Props> = props => {
  return (
    <Styles className="FourZeroFour">
      <header className="FourZeroFour__header" />
      <main className="FourZeroFour__main">
        <FourZeroFourSVG className="FourZeroFour__icon" />
      </main>
      <footer className="FourZeroFour__footer" />
    </Styles>
  );
};

FourZeroFour.defaultProps = {};

export default FourZeroFour;
