import React from "react";
import { render } from "@testing-library/react";

import FourZeroFour from "./404.page";

describe("404 page", () => {
  it("renders without crashing", () => {
    render(<FourZeroFour />);
  });
});
