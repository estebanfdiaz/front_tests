import React from "react";

import { HomeProps as Props } from "./Home.page.types";
import HomeContainer from "containers/home/HomeContainer/HomeContainer";
import SubRouter from "components/global/SubRouter/SubRouter";
import { routes } from "./Home.page.routes";

const Home: React.FC<Props> = () => {
  return (
    <HomeContainer>
      <SubRouter url="/" routes={routes} />
    </HomeContainer>
  );
};

Home.defaultProps = {};

export default Home;
