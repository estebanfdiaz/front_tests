import React, { lazy } from "react";

import { SingleRoute } from "../../components/global/SubRouter/SubRouter.types";

const Intro = lazy(() => import("components/home/Home/Intro/Intro"));

export const routes: SingleRoute[] = [
  {
    path: "/",
    component: () => <Intro />,
    exact: true,
    isPrivate: true
  }
];
