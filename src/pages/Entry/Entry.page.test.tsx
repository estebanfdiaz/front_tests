import React from "react";
import { render } from "setupTests";

import Entry from "./Entry.page";

describe("Entry page", () => {
  it("renders without crashing", () => {
    render(<Entry component={() => <div>Home</div>} />);
  });
});
