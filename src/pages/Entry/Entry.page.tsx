import React, { useEffect } from "react";
import { Redirect } from "react-router-dom";

import { EntryProps as Props } from "./Entry.page.types";
import { isLoggedIn } from "redux/auth/auth.actions";
import CONSTANTS from "config/constants";
import { useSelector, useDispatch } from "redux/store";

const Entry: React.FC<Props> = props => {
  const { component } = props;
  const dispatch = useDispatch();
  const loggedIn = useSelector(state => state.Auth.loggedIn);

  useEffect(() => {
    dispatch(isLoggedIn());
  }, [dispatch]);

  if (typeof loggedIn === "undefined") {
    return null;
  } else if (!loggedIn) {
    return <Redirect to={CONSTANTS.NO_AUTH_PATH} />;
  } else {
    return component();
  }
};

Entry.defaultProps = {};

export default Entry;
