// Interfaces and types from component Entry

// Container Props
export interface EntryProps {
  component: () => JSX.Element;
}
