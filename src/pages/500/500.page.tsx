import React from "react";
import { useHistory } from "react-router-dom";

import Styles from "./500.styles.page";
import { FiveZeroZeroProps as Props } from "./500.page.types";
import Button from "components/global/Button/Button";

import { ReactComponent as FiveZeroZeroSVG } from "assets/images/500/500.svg";

const FiveZeroZero: React.FC<Props> = props => {
  const { push } = useHistory();
  const reload = () => {
    push("/");
    window.location.reload();
  };

  return (
    <Styles className="FiveZeroZero">
      <header className="FiveZeroZero__header" />
      <main className="FiveZeroZero__main">
        <div className="FiveZeroZero__wrapper">
          <FiveZeroZeroSVG className="FiveZeroZero__icon" />
          <Button className="FiveZeroZero__cta" onClick={reload}>
            Recargar
          </Button>
        </div>
      </main>
      <footer className="FiveZeroZero__footer" />
    </Styles>
  );
};

FiveZeroZero.defaultProps = {};

export default FiveZeroZero;
