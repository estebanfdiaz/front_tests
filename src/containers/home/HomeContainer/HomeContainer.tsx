import React from "react";

import { HomeContainerProps as Props } from "./HomeContainer.types";
import Home from "components/home/Home/Home";

const HomeContainer: React.FC<Props> = props => {
  return <Home>{props.children}</Home>;
};

HomeContainer.defaultProps = {};

export default HomeContainer;
